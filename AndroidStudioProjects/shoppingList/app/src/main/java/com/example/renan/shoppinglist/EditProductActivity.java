package com.example.renan.shoppinglist;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.provider.MediaStore;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.TextView;
import android.widget.Toast;

public class EditProductActivity extends Activity {
    public static final int CAMERA_REQUEST_CODE = 0;
    private Button cancel_Btn;
    private Button ok_Btn;
    private Button deleteBtn;
    private ImageButton prodcut_Img_btn;
    private Bitmap bitmap;
    int position;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_edit_product);

        TextView nameTv = findViewById(R.id.name_product_e);
        TextView amountTv = findViewById(R.id.amount_product_e);
        TextView costTv =  findViewById(R.id.Price_Product_e);
        prodcut_Img_btn = findViewById(R.id.image_btn_e);

        position = Integer.parseInt(getIntent().getStringExtra("position"));
        final Product selectedProduct = ProductsList.getInstance().getList().get(position);

        nameTv.setText(selectedProduct.getM_Name());
        amountTv.setText(selectedProduct.getM_amount());
        costTv.setText(selectedProduct.getM_PricePerUint());
        prodcut_Img_btn.setImageBitmap(selectedProduct.getM_bitmap());

        cancel_Btn = findViewById(R.id.cancel_btn_e);
        cancel_Btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(EditProductActivity.this,MainActivity.class);
                intent.setFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP|Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(intent);
            }
        });

        ok_Btn = findViewById(R.id.ok_btn_e);
        ok_Btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                EditText productName_Et = findViewById(R.id.name_product_e);
                EditText amountProduct_Et = findViewById(R.id.amount_product_e);
                EditText Price_Product_Et = findViewById(R.id.Price_Product_e);

                String name = productName_Et.getText().toString();
                String amount = amountProduct_Et.getText().toString();
                String Price = Price_Product_Et.getText().toString();

                Intent intent = new Intent(EditProductActivity.this,MainActivity.class);
                selectedProduct.setM_Name(name);
                selectedProduct.setM_amount(amount);
                selectedProduct.setM_PricePerUint(Price);
                selectedProduct.setM_bitmap(bitmap);

                intent.setFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP|Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(intent);
            }
        });

        prodcut_Img_btn = findViewById(R.id.image_btn_e);
        prodcut_Img_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                startActivityForResult(intent,CAMERA_REQUEST_CODE);
            }
        });

        deleteBtn = findViewById(R.id.delete_btn_e);
        deleteBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AlertDialog.Builder builder = new AlertDialog.Builder(EditProductActivity.this);
                builder.setCancelable(true);
                builder.setTitle(R.string.alert);
                builder.setMessage(R.string.delete);

                builder.setNegativeButton(R.string.cancel, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.cancel();
                    }
                });

                builder.setPositiveButton(R.string.Ok_btn, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {

                        ProductsList.getInstance().getList().remove(position);
                        ProductsList.getInstance().updateProgressBar();
                        ProductsList.getInstance().calcTotalPrice();
                        Intent intent = new Intent(EditProductActivity.this,MainActivity.class);
                        intent.setFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP|Intent.FLAG_ACTIVITY_CLEAR_TOP);
                        startActivity(intent);
                        Toast.makeText(EditProductActivity.this, R.string.deleted, Toast.LENGTH_SHORT).show();
                    }
                });
                builder.show();
            }
        });

    }
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        switch (requestCode){
            case CAMERA_REQUEST_CODE:
                        if(data != null && data.getExtras()!=null){
                            Bitmap bitmap = (Bitmap) data.getExtras().get("data");
                            this.bitmap = bitmap;
                            prodcut_Img_btn.setImageBitmap(bitmap);
                }
                break;


        }


    }
}
