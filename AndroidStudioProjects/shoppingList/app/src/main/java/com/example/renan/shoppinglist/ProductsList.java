package com.example.renan.shoppinglist;

import java.util.ArrayList;

public class ProductsList
{
    private static ProductsList instance;
    private ArrayList<Product> list;

    private ProductsList(){
        list = new ArrayList<Product>();
    }

    public ArrayList<Product> getList(){
        return list;
    }

    public static ProductsList getInstance(){
        if (instance == null)
        {
            instance = new ProductsList();
        }
        return instance;
    }
    public int calcTotalPrice(){
        int total = 0;
        for (int i=0;i<list.size();i++){
            if(!list.get(i).getM_amount().equalsIgnoreCase("") && !list.get(i).getM_PricePerUint().equalsIgnoreCase(""))
            {
                total  += Integer.parseInt(list.get(i).getM_amount()) * Integer.parseInt(list.get(i).getM_PricePerUint());
            }
        }
        return total;
    }

    public int updateProgressBar(){
        int numOfChecked = 0;
        for (int i=0;i<list.size();i++){
            if(list.get(i).isM_IsCheck()){
                numOfChecked += (((float)1/(float)list.size())*100);
            }
        }
        return numOfChecked;
    }
}
