package com.example.renan.shoppinglist;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.ImageButton;
import android.widget.ProgressBar;
import android.widget.TextView;
import java.util.List;

public class ProductAdapter extends BaseAdapter implements CompoundButton.OnCheckedChangeListener, View.OnLongClickListener {

    private Context m_Context;
    private List<Product> products;
    private ProgressBar progressBar;
    private  TextView nameTv;
    private TextView amountTv;
    private  TextView costTv;

    public ProductAdapter(Context m_Context, List<Product> products,ProgressBar progressBar) {
        this.m_Context = m_Context;
        this.products = products;
        this.progressBar = progressBar;
    }

    @Override
    public int getCount() {
        return products.size();
    }

    @Override
    public Object getItem(int position) {
        return products.get(position);
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent)
    {
        View row = convertView;
        if(row == null){
            LayoutInflater inflater = (LayoutInflater)m_Context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            row = inflater.inflate(R.layout.details_product,parent,false);
        }

        Product product = products.get(position);

        nameTv = row.findViewById(R.id.name_product);
        amountTv = row.findViewById(R.id.amount_product);
        costTv =  row.findViewById(R.id.Price_Product);
        ImageButton prodcut_Img_btn = row.findViewById(R.id.image_btn);
        CheckBox checkBox = row.findViewById(R.id.is_checked);
        checkBox.setOnCheckedChangeListener(this);
        checkBox.setTag(position);

        row.setOnLongClickListener(this);
        row.setTag(position);


        nameTv.setText(product.getM_Name());
        amountTv.setText(product.getM_amount());
        costTv.setText(product.getM_PricePerUint());
        checkBox.setChecked(product.isM_IsCheck());
        prodcut_Img_btn.setImageBitmap(product.getM_bitmap());

        return row;
    }
    protected void updateProgressBar(){
        int numOfChecked = 0;
        for (int i=0;i<products.size();i++){
            if(products.get(i).isM_IsCheck()){
                numOfChecked += (((float)1/(float)products.size())*100);
            }
        }
        progressBar.setProgress(numOfChecked);
    }
    @Override
    public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
        int position = (Integer)buttonView.getTag();
        Product product = products.get(position);
        product.setM_IsCheck(isChecked);

        updateProgressBar();
    }

    @Override
    public boolean onLongClick(View v) {
        Intent intent = new Intent(m_Context,EditProductActivity.class);
        Product product = (Product)this.getItem(0);
        intent.putExtra("position",v.getTag().toString() );
        m_Context.startActivity(intent);

        return false;
    }
}
