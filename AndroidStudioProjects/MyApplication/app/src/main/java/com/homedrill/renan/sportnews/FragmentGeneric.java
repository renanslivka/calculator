package com.homedrill.renan.sportnews;

import android.annotation.SuppressLint;
import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.telephony.TelephonyManager;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.homedrill.renan.sportnews.api.ApiClient;
import com.homedrill.renan.sportnews.api.ApiInterface;
import com.homedrill.renan.sportnews.models.Article;
import com.homedrill.renan.sportnews.models.News;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static android.content.Context.ALARM_SERVICE;

@SuppressLint("ValidFragment")
public class FragmentGeneric extends Fragment implements SwipeRefreshLayout.OnRefreshListener{
    public static final String API_KEY = "781fe53ce8f74fc48f3e57d70db1088b";
    private List<Article> articles;
    private RecyclerView recyclerView;
    private Context context;
    private Adapter adapter;
    private String category;
    private SwipeRefreshLayout swipeRefreshLayout;
    private View fragment_view;

    @SuppressLint("ValidFragment")
    public FragmentGeneric(List<Article> articleList, Context context, String category) {
        this.articles = articleList;
        this.context = context;
        this.category = category;
    }

    private void initView() {
        recyclerView =  fragment_view.findViewById(R.id.recycler);
        swipeRefreshLayout =  fragment_view.findViewById(R.id.swipe_refresh_layout);
        swipeRefreshLayout.setOnRefreshListener(this);
    }

    private void setAdapter() {
        recyclerView.setLayoutManager(new LinearLayoutManager(this.getActivity()));
        adapter = new Adapter( articles,getActivity());
        recyclerView.setAdapter(adapter);
    }


    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        setHasOptionsMenu(true);
        fragment_view = inflater.inflate(R.layout.general_news_fragment, null);
        initView();
        setAdapter();
        loadJson();
        return fragment_view;
    }

    public void loadJson(){
        swipeRefreshLayout.setRefreshing(true);
        final ApiInterface apiInterface = ApiClient.getApiClient().create(ApiInterface.class);

        //String country = Utils.getCountry();//take the country with the current keyboard
        TelephonyManager tm = (TelephonyManager)context.getSystemService(Context.TELEPHONY_SERVICE);//take the country with the current connected network
        String country = tm.getNetworkCountryIso();

        Call<News> call = apiInterface.getGenericNews(country, category,API_KEY);

        call.enqueue(new Callback<News>() {
            @Override
            public void onResponse(Call<News> call, Response<News> response) {
                if (response.isSuccessful()){
                    if (!articles.isEmpty()){
                        articles.clear();
                    }
                    articles = response.body().getArticles();
                    adapter = new Adapter(articles,context);
                    recyclerView.setAdapter(adapter);
                    adapter.notifyDataSetChanged();

                    initListener();
                    swipeRefreshLayout.setRefreshing(false);
                }
                else {
                    Toast.makeText(context, "No Result!", Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<News> call, Throwable t) {
                swipeRefreshLayout.setRefreshing(false);
            }
        });
    }


    private void initListener(){
        adapter.setOnItemClickListener(new Adapter.OnItemClickListener() {
            @Override
            public void onItemClick(View view, int position) {
                Intent intent = new Intent(context,NewsDetailActivity.class);

                Article article = articles.get(position);
                intent.putExtra("url", article.getUrl());
                intent.putExtra("title", article.getTitle());
                intent.putExtra("img", article.getUrlToImage());
                intent.putExtra("date", article.getPublishedAt());
                intent.putExtra("source", article.getSource().getName());
                intent.putExtra("author", article.getAuthor());


                startActivity(intent);
            }
        });
    }


    @Override
    public void onRefresh() {
        loadJson();
    }
}
